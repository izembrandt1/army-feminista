import discord
from discord.ext import commands
from decouple import config
import tweepy
import random
from credenciales import *
import wikipedia
import requests
import json

#Bot de Wikipedia (imagenes)
WIKI_REQUEST = 'http://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles='

def get_wiki_image(search_term):
    try:
        result = wikipedia.search(search_term, results = 1)
        wikipedia.set_lang('en')
        wkpage = wikipedia.WikipediaPage(title = result[0])
        title = wkpage.title
        response  = requests.get(WIKI_REQUEST+title)
        json_data = json.loads(response.text)
        img_link = list(json_data['query']['pages'].values())[0]['original']['source']
        return img_link        
    except:
        return "Ups, no se encontró la imagen de tu bae :c"

#Bot de Twitter (funas)
#Credenciales de twitter
auth = tweepy.OAuthHandler(API_KEY, API_SECRET_KEY)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

#Creacion de API
api = tweepy.API(auth)

#Conexion 
try:
    api.verify_credentials()
    print("Authentication OK")
except:
    print("Error during authentication")

#Biblioteca funas
acciones=['tiro fosforito por su pie','le hizo bullying al llamarle calienta pijas','acuchilló el riñón izquierdo con un destornillador',
'quitó su plata del almuerzo','tanteó',' masajeó el escroto','ofreció una bolsa de coquito por sus servicios sexuales',
'embarazo y no quizo hacerse cargo','se adelanto en la fila para la vacunacion']

victimas=['sus compañeros de facultad','su familia','su vecino','unos indígenas','un grupo de mujeres','un grupo de mujeres trans',
'su mejor amigo Hugo Fletes','profesor de Ecologia','cajera del super biggies','limpiavidrio','grupo de minonitas','una chica de las ARMY',
'Herbert','un chofer de la linea 27','un youtuber']

lugares=['la ciclovía de San Lorenzo',' el puente remanso','la orilla del río Ypané','el famoso prostíbulo Las Diablitas',
'La placita del barrio donde se venden estupefacientes','la cancha de Cerro Porteño','el helipuerto de la facultad Politécnica de la UNA',
'balneario Veracruz','Chechos la mega','el Poniente','Shopping VIllamorra','Mundo aparte','Barrio Obrero','Popeyes','Barbarella',
'la Comisaria','el calabozo de tacumbu','el buen pastor','la casa de su vecino','cine Itau Pinedo']

uniones=['encima','inclusive','pueden creer que','y despues','y tuvo el descaro','y aunque cueste creer','y si pensaron que eso era malo',
'luego me entere','no podran creer que','el hijo de puta encima','imperdonable porque encima']

punchlines=['no me devolvió mi 5000 que le di para su pasaje','siempre me tanteaba','le pisó la cola a mi gato kaure',
'hizo trampa en cálculo para pasar, ni siquiera sabe integrar','quizo comerle a su prima','quizo robar su celular a un limpiavidrio',
'puso toddy por el envase de ketchup','fumo marihuana en la iglesia','vio porno en el colectivo','robo un kilo de banana del super',
'se vacuno 3 veces ya contra el covid']

hashtags=" #funa #cancelado #BTS #ARMY #metoo"

#Biblioteca cancelaciones
cancelaciones=['ingresó sin tapabocas a IPS INGAVI y exigió al guardia de seguridad que le lleve junto a "la enfermera vaira que tiene feroz culo" Cuando el guardia le pidió que se ponga un tapabocas el hombre lo agredió proporcionándole varios golpes en el rostro y en la costilla izquierda, si tienen alguna información sobre su identidad o paradero por favor necesitamos AYUDA!!!',
'pio no era al que le funaron porque antes cuando trabajaba como profesor de educación física  le tocaba la cola a las nenas en el colegio',
'fue encarcelado por haber irrumpido en una oficina estatal, luego de esperar cerca de quince minutos por un trámite burocrático,  alegando que su padre es un senador mientras exigió a la secretaria que "Le haga un pete" por las molestias ocasionadas si no quería perder su trabajo',
'es acusado de liderar red de captación de víctimas de una peligrosa red de trata en la triple frontera',
'es llevado a Tacumbú luego de haberse encontrado más de 20gb de pornografía infantil en su teléfono celular',
'habría engañado a su prima haciéndose pasar por su novio para pedirle fotos íntimas. Delitos Informáticos declaró que están intentando desbloquear su teléfono',
'es acusado por más de diez mujeres de haberse masturbado frente a ellas.',
'fue encarcelado luego de que sus huellas dactilares hayan sido encontradas en el cadáver de una mujer indígena menor de edad.',
'fue atrapado por la policía mientras estaba estacionado en las inmediaciones de un jardín de niños, sin ropa, en una furgoneta blanca.',
'fue acusado de manosear a sus compañeras de trabajo a cambio de buenas referencias laborales.',
'le hizo creer a varias mujeres que tiene una agencia de modelaje y les pidió favores sexuales a cambio de tener éxito en el rubro',
'golpeó a su pareja porque el bife a caballo estaba frío',
'emborrachó a su pareja para que "por fin le entregue el marrón"',
'le emborrachó a una amistad y le hizo creer que es bisexual',
'creó un OnlyFans con las fotos íntimas que su ex le había confiado hace un buen tiempo atrás.',
'engañó a una menor para que le preste la tarjeta de crédito de su mamá',
'emborracha mujeres para robarles la billetera']

#Funciones
def funa_ia():
	msj_funa = " le " + random.choice(acciones) + " a " + random.choice(victimas) + " en " + random.choice(lugares) + " " + random.choice(uniones) + " " + random.choice(punchlines) + hashtags
	return msj_funa

def funar(funado):
	return tweetear(funado + funa_ia())

def cancelar(cancelado):
	return tweetear(cancelado+" "+ random.choice(cancelaciones)+"\n"+hashtags)

def link_ultimo_tweet():
	last_tweet = api.user_timeline(api.me(), count = 1)
	link = "https://twitter.com/"+api.me().screen_name+"/status/"+str(last_tweet[0].id)
	print(link)
	return link

def tweetear(mensaje):
	try:
		if api.update_status(mensaje):
			print("tweet exitoso")
			return link_ultimo_tweet()
	except tweepy.error.TweepError as e:
		print("uh oh")
	return "funa fallida"

#Bot de Discord
client = discord.Client()

bot = commands.Bot(command_prefix='!')

@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))

@bot.command(name='funar')
async def _funar(ctx,*,nombre_funado):
    await ctx.channel.send('Te llegó la hora '+nombre_funado+', vas a ser funado por las ARMY')
    link = funar(nombre_funado)
    await ctx.channel.send(link)

@bot.command(name='cancelar')
async def _cancelar(ctx,*,nombre_cancelado):
    await ctx.channel.send('Te llegó la hora '+nombre_cancelado +', perdiste tus privilegios de existir')
    link = cancelar(nombre_cancelado)
    await ctx.channel.send(link)

@bot.command(name='ayuda')
async def _ayuda(ctx):
    embed_message = discord.Embed(title="Comandos de ARMY FEMINISTA",description="Cada comando recibe una palabra o una oración para funcionar (a menos que se especifique lo contrario), y tienen el siguiente formato: !<palabra> <oración>")
    embed_message.add_field(name="!funar",value="Funa generada aleatoriamente. Destruye la vida de la persona que se metió con nuestros oppas u_u")
    embed_message.add_field(name="!cancelar",value="Funa tomada de una lista. Esa persona nunca más osará burlarse de BTS muajajja o3o")
    embed_message.add_field(name="!fandom",value="Úsalo para agregar a alguien a nuestro hermoso fandom c;")
    embed_message.add_field(name="!ayuda",value="Este comando no recibe palabras ni oraciones. Envíalo al canal de texto para obtener ayuda sobre cómo usar este y otros comandos c:")
    await ctx.channel.send(embed = embed_message)


@bot.command(name='fandom')
async def _foto(ctx,*,busqueda):
    imagen = get_wiki_image(busqueda)
    await ctx.channel.send("Confirmado: "+busqueda+" es ARMY")
    await ctx.channel.send(imagen)

bot.run(config('BOT_KEY'))

